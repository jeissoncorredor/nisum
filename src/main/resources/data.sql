/*==============================================================*/
/* Table: phone                                                 */
/*==============================================================*/
create table phone
(
    id                   bigint not null auto_increment,
    use_id               uuid not null,
    number               varchar(20) not null,
    city_code            varchar(2) not null,
    country_code         varchar(2) not null,
    primary key (id)
);

/*==============================================================*/
/* Table: user                                                  */
/*==============================================================*/
create table user
(
    id                   uuid not null,
    name                 text not null,
    password             text not null,
    email                text not null,
    created              timestamp not null,
    modified             timestamp,
    last_login           timestamp not null,
    token                text,
    is_active            bool not null,
    primary key (id)
);

alter table phone add constraint fk_user_phone foreign key (use_id)
    references user (id) on delete restrict on update restrict;