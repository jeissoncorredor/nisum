package com.nisum.mapper;

import com.nisum.dto.PhoneDto;
import com.nisum.model.Phone;

import java.util.UUID;

public class PhoneMapper {

    public static Phone phoneDtoToPhone(PhoneDto phoneDto, UUID userId) {
        Phone phone = new Phone();
        phone.setCityCode(phoneDto.getCitycode());
        phone.setCountryCode(phoneDto.getCountrycode());
        phone.setNumber(phoneDto.getNumber());
        phone.setUserId(userId);
        return phone;
    }

}
