package com.nisum.mapper;

import com.nisum.dto.UserDto;
import com.nisum.model.User;
import com.nisum.util.Encrypt;

import java.util.UUID;

public class UserMapper {

    public static User userDtoToUser(UserDto userDto) {
        User user = new User();
        user.setId(UUID.randomUUID());
        user.setName(userDto.getName());
        user.setPassword(Encrypt.encodeMd5(userDto.getPassword()));
        user.setEmail(userDto.getEmail());
        return user;
    }

    public static UserDto userToUserDto(User user) {
        UserDto userDto = new UserDto();
        userDto.setId(user.getId());
        userDto.setName(user.getName());
        userDto.setEmail(user.getEmail());
        userDto.setToken(user.getToken());
        if (user.getActive() != null) {
            userDto.setActive(user.getActive());
        }
        userDto.setCreated(user.getCreated());
        userDto.setModified(user.getModified());
        userDto.setLastLogin(user.getLastLogin());
        return userDto;
    }
}
