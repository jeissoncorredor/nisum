package com.nisum.util;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.apache.commons.codec.binary.Hex;

public class Encrypt {
    public static String encodeMd5(String value) {
        try {
            if (value != null && !value.trim().equals("")) {
                MessageDigest messageDigest;
                messageDigest = MessageDigest.getInstance("MD5");
                messageDigest.reset();
                messageDigest.update(value.getBytes(StandardCharsets.UTF_8));
                byte[] resultByte = messageDigest.digest();
                return Hex.encodeHexString(resultByte);
            } else {
                return "";
            }
        } catch (NoSuchAlgorithmException ex) {
            return "";
        }
    }

}
