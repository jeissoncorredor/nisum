package com.nisum.util;

import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import io.jsonwebtoken.Jwts;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Component
public class JWTGenerator {

    @Value("${jwt.time}")
    private Long time;

    @Value("${jwt.time}")
    private String secret;

    /**
     * This method create a jwt token
     *
     * @param email users mail
     * @return You will get a valid JWT Token
     */
    public String generateToken(String email) {

        long currentTime = System.currentTimeMillis();
        Map<String, Object> params = new HashMap<String, Object>() {{
            put("email", email);
            put("typ", "Bearer");
        }};

        return Jwts.builder().signWith(SignatureAlgorithm.HS256, secret)
                .setClaims(params)
                .setIssuedAt(new Date(currentTime))
                .setHeaderParam("typ", "JWT")
                .setExpiration(new Date(currentTime + (time)))
                .compact();
    }
}
