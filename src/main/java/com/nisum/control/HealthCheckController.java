package com.nisum.control;

import com.nisum.route.InternalRoute;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;

@RestController
@RequestMapping(value = InternalRoute.BASE)
public class HealthCheckController {

    @GetMapping(value = InternalRoute.HEALTH)
    public ResponseEntity health() {
        return ResponseEntity.status(HttpStatus.OK).body(
                new HashMap<String, String>() {{
                    put("status", "UP");
                }});
    }
}
