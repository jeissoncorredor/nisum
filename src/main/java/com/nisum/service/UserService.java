package com.nisum.service;

import com.nisum.dto.UserDto;
import com.nisum.exception.MsException;
import com.nisum.mapper.PhoneMapper;
import com.nisum.mapper.UserMapper;
import com.nisum.model.User;
import com.nisum.repository.PhoneRepository;
import com.nisum.repository.UserRepository;
import com.nisum.util.JWTGenerator;
import com.nisum.validation.ModelValidation;

import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.sql.Timestamp;
import java.time.LocalDateTime;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PhoneRepository phoneRepository;

    @Autowired
    private JWTGenerator jwtGenerator;

    @Transactional
    public UserDto save(UserDto userDto) {
        try {
            validationsOnSave(userDto);
            User user = UserMapper.userDtoToUser(userDto);
            user.setCreated(Timestamp.valueOf(LocalDateTime.now()));
            user.setLastLogin(user.getCreated());
            user.setToken(jwtGenerator.generateToken(userDto.getEmail()));
            user.setActive(true);
            user = userRepository.save(user);
            if (userDto.getPhones() != null) {
                final UUID userId = user.getId();
                userDto.getPhones().forEach(phone -> phoneRepository.save(PhoneMapper.phoneDtoToPhone(phone, userId)));
            }
            return UserMapper.userToUserDto(user);
        } catch (Exception ex) {
            if (ex.getClass() == MsException.class) {
                throw ex;
            } else {
                throw new MsException(ModelValidation.INVALID_DATA);
            }
        }
    }

    private void validationsOnSave(UserDto userDto) {
        // Validate if the mail is taken
        if (userRepository.existUsersWithMail(userDto.getEmail())) {
            throw new MsException(ModelValidation.DUPLICATE_MAIL);
        }
    }
}
