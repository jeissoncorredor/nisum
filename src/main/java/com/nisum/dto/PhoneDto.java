package com.nisum.dto;

import com.nisum.validation.ModelValidation;

import javax.persistence.Column;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

public class PhoneDto {
    @NotBlank(message = ModelValidation.NOT_BLANK)
    @Size(max = 20, message = ModelValidation.SIZE + " (20)")
    String number;

    @NotBlank(message = ModelValidation.NOT_BLANK)
    @Column(name = "city_code")
    @Size(max = 2, message = ModelValidation.SIZE + " (2)")
    String citycode;

    @NotBlank(message = ModelValidation.NOT_BLANK)
    @Size(max = 2, message = ModelValidation.SIZE + " (2)")
    String countrycode;

    public PhoneDto(String number, String citycode, String countryCode) {
        this.number = number;
        this.citycode = citycode;
        this.countrycode = countryCode;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getCitycode() {
        return citycode;
    }

    public void setCitycode(String citycode) {
        this.citycode = citycode;
    }

    public String getCountrycode() {
        return countrycode;
    }

    public void setCountrycode(String countrycode) {
        this.countrycode = countrycode;
    }
}
