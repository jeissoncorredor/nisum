package com.nisum.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class PasswordConstraintValidator implements ConstraintValidator<Password, String> {

    public boolean isValid(String password, ConstraintValidatorContext cvc) {
        char pass;
        boolean containsUpper = false;
        boolean containsLower = false;
        int numbers = 0;
        for (byte i = 0; i < password.length(); i++) {
            pass = password.charAt(i);
            String passValue = String.valueOf(pass);
            if (containsLower && containsUpper && numbers >= 2) {
                break;
            } else {
                if (passValue.matches("[A-Z]")) {
                    containsUpper = true;
                } else if (passValue.matches("[a-z]")) {
                    containsLower = true;
                } else if (passValue.matches("[0-9]")) {
                    numbers++;
                }
            }
        }
        return containsUpper && containsLower && numbers >= 2;
    }
}