package com.nisum.validation;

public class ModelValidation {

    public static final String NOT_BLANK = "El campo no puede ser vacio.";
    public static final String INVALID_MAIL = "El correo ingresado no tiene el formato correcto.";
    public static final String INVALID_PASSWORD = "La clave debe contener una mayuscula, letras minúsculas, y dos numeros.";
    public static final String DUPLICATE_MAIL = "El correo ya registrado.";
    public static final String INVALID_DATA = "Datos inválidos.";
    public static final String SIZE = "El tamaño máximo permito fue superado. Máximo permitido";

}
