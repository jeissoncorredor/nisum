package com.nisum.route;

public class InternalRoute {
    public static final String BASE = "/security";
    public static final String HEALTH = "/health";
    public static final String USER = InternalRoute.BASE + "/v1/users";
    public static final String BY_ID = "/{id}";

}
