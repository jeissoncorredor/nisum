package com.nisum.exception;

public class MsException extends RuntimeException {
    private String message;

    public MsException(String message) {
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
