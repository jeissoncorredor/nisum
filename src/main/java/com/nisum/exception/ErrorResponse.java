package com.nisum.exception;

public class ErrorResponse {
    Object message;

    public ErrorResponse(Object message) {
        this.message = message;
    }

    public Object getMessage() {
        return message;
    }

    public void setMessage(Object message) {
        this.message = message;
    }
}
