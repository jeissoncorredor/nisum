package com.nisum.exception;

import java.util.ArrayList;

import com.nisum.validation.ModelValidation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class HandlerException {

    @ExceptionHandler(HttpMessageNotReadableException.class)
    public ResponseEntity<ErrorResponse> validateInputRequest(HttpMessageNotReadableException exception) {
        ErrorResponse response;
        if (exception.getCause() != null) {
            response = new ErrorResponse(exception.getCause().getMessage());
        } else {
            response = new ErrorResponse(ModelValidation.INVALID_DATA);
        }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<ErrorResponse> validateFieldEntity(MethodArgumentNotValidException exception) {
        ArrayList<FieldValidation> fields = new ArrayList<>();
        exception.getBindingResult().getFieldErrors().forEach(it -> fields.add(new FieldValidation(it.getField(), it.getDefaultMessage())));
        ErrorResponse response = new ErrorResponse(fields);
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
    }


    @ExceptionHandler(MsException.class)
    public ResponseEntity<ErrorResponse> validationErrorHandler(MsException ex) {
        ErrorResponse response = new ErrorResponse(ex.getMessage());
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
    }
}
