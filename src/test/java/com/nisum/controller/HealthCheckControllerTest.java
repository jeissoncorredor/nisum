package com.nisum.controller;

import com.nisum.control.HealthCheckController;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

@SpringBootTest
public class HealthCheckControllerTest {

    @InjectMocks
    private HealthCheckController healthCheckController;

    @Test
    void health() {
        ResponseEntity response = healthCheckController.health();
        Assertions.assertEquals(response.getStatusCode(), HttpStatus.OK);
    }
}
