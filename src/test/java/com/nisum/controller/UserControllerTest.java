package com.nisum.controller;

import com.nisum.control.UserController;
import com.nisum.dto.UserDto;
import com.nisum.service.UserService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Objects;

@SpringBootTest
public class UserControllerTest {

    @InjectMocks
    private UserController userController;

    @Mock
    private UserService userService;

    @Test
    void save() {
        UserDto userDto = new UserDto();
        userDto.setEmail("aaa@hotmail.com");
        Mockito.when(userService.save(userDto)).thenReturn(userDto);
        ResponseEntity<UserDto> response = userController.save(userDto);
        Assertions.assertEquals(response.getStatusCode(), HttpStatus.CREATED);
        Assertions.assertEquals(Objects.requireNonNull(response.getBody()).getEmail(), userDto.getEmail());
    }
}
