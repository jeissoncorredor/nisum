package com.nisum.mapper;

import com.nisum.dto.PhoneDto;
import com.nisum.model.Phone;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.UUID;

@SpringBootTest
public class PhoneMapperTest {

    @Test
    void phoneDtoToPhone() {
        UUID userId = UUID.randomUUID();
        PhoneDto phoneDto = new PhoneDto("123", "11", "22");
        Phone phone = PhoneMapper.phoneDtoToPhone(phoneDto, userId);
        Assertions.assertEquals(phone.getCityCode(), phoneDto.getCitycode());
        Assertions.assertEquals(phone.getCountryCode(), phoneDto.getCountrycode());
        Assertions.assertEquals(phone.getNumber(), phoneDto.getNumber());
        Assertions.assertEquals(phone.getUserId(), userId);
    }
}
