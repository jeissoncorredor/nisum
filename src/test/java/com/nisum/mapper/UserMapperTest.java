package com.nisum.mapper;

import com.nisum.dto.UserDto;
import com.nisum.model.User;
import com.nisum.util.Encrypt;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class UserMapperTest {

    @Test
    void userDtoToUser() {
        UserDto userDto = new UserDto();
        userDto.setName("My name");
        userDto.setEmail("aaa@hotmail.com");
        userDto.setPassword("123");
        User user = UserMapper.userDtoToUser(userDto);
        Assertions.assertEquals(userDto.getName(), user.getName());
        Assertions.assertEquals(userDto.getEmail(), user.getEmail());
        Assertions.assertEquals(Encrypt.encodeMd5(userDto.getPassword()), user.getPassword());
        Assertions.assertNotNull(user.getId());
    }

    @Test
    void userToUserDto() {
        User user = new User();
        user.setName("My name");
        user.setEmail("aaa@hotmail.com");
        user.setPassword("123");
        UserDto userDto = UserMapper.userToUserDto(user);
        Assertions.assertEquals(userDto.getName(), user.getName());
        Assertions.assertEquals(userDto.getEmail(), user.getEmail());
        Assertions.assertNull(userDto.getPassword());
    }

    @Test
    void userToUserDtoActive() {
        User user = new User();
        user.setName("My name");
        user.setEmail("aaa@hotmail.com");
        user.setPassword("123");
        user.setActive(true);
        UserDto userDto = UserMapper.userToUserDto(user);
        Assertions.assertEquals(userDto.getName(), user.getName());
        Assertions.assertEquals(userDto.getEmail(), user.getEmail());
        Assertions.assertNull(userDto.getPassword());
    }
}
