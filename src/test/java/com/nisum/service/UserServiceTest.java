package com.nisum.service;

import com.nisum.dto.PhoneDto;
import com.nisum.dto.UserDto;
import com.nisum.exception.MsException;
import com.nisum.model.Phone;
import com.nisum.model.User;
import com.nisum.repository.PhoneRepository;
import com.nisum.repository.UserRepository;
import com.nisum.util.JWTGenerator;
import com.nisum.validation.ModelValidation;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.UUID;

@SpringBootTest
public class UserServiceTest {
    @InjectMocks
    UserService userService;

    @Mock
    UserRepository userRepository;

    @Mock
    PhoneRepository phoneRepository;

    @Mock
    JWTGenerator jwtGenerator;

    @Test
    void createUser() {
        UserDto userDto = new UserDto();
        userDto.setEmail("mimail@anymail.com");
        userDto.setPassword("MyPass123");
        userDto.setName("My name");
        ArrayList<PhoneDto> phones = new ArrayList<>();
        phones.add(new PhoneDto("123", "11", "57"));
        userDto.setPhones(phones);

        User user = new User();
        user.setId(UUID.randomUUID());
        user.setEmail(userDto.getEmail());
        user.setName(userDto.getName());
        user.setActive(true);
        user.setToken("any_token");

        Mockito.when(userRepository.save(Mockito.any(User.class))).thenReturn(user);
        Mockito.when(phoneRepository.save(Mockito.any(Phone.class))).thenReturn(new Phone());
        Mockito.when(jwtGenerator.generateToken(userDto.getEmail())).thenReturn("any_token");

        UserDto userDtoResponse = userService.save(userDto);

        Assertions.assertEquals(userDtoResponse.getId(), user.getId());
        Assertions.assertEquals(userDtoResponse.getEmail(), userDto.getEmail());
        Assertions.assertEquals(userDtoResponse.getName(), userDto.getName());
        Assertions.assertNotNull(userDtoResponse.getToken());
        Assertions.assertNull(userDtoResponse.getPassword());
        Assertions.assertNull(userDtoResponse.getPhones());
    }

    @Test
    void duplicateMail() {
        UserDto userDto = new UserDto();
        userDto.setEmail("mimail@anymail.com");

        Mockito.when(userRepository.existUsersWithMail(userDto.getEmail())).thenReturn(true);
        MsException exception = Assertions.assertThrows(
                MsException.class,
                () -> userService.save(userDto)
        );
        Assertions.assertEquals(exception.getMessage(), ModelValidation.DUPLICATE_MAIL);
    }

    @Test
    void invalidData() {
        MsException exception = Assertions.assertThrows(
                MsException.class,
                () -> userService.save(new UserDto())
        );
        Assertions.assertEquals(exception.getMessage(), ModelValidation.INVALID_DATA);
    }
}
