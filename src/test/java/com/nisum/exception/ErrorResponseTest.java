package com.nisum.exception;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class ErrorResponseTest {

    ErrorResponse objectTest = new ErrorResponse("OK");

    @Test
    void returnErrorResponse() {
        Assertions.assertEquals("OK", objectTest.message);
    }
}
