package com.nisum.exception;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class MsExceptionTest {

    MsException objectTest = new MsException("any");

    @Test
    void returnMsException() {
        Assertions.assertEquals("any", objectTest.getMessage());
    }

}
