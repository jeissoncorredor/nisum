package com.nisum.validation;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class PasswordConstraintValidatorTest {

    @Test
    void checkPass() {
        PasswordConstraintValidator passwordConstraintValidator = new PasswordConstraintValidator();
        Assertions.assertTrue(passwordConstraintValidator.isValid("Main12", null));
        Assertions.assertFalse(passwordConstraintValidator.isValid("Main1", null));
        Assertions.assertFalse(passwordConstraintValidator.isValid("main12", null));
        Assertions.assertFalse(passwordConstraintValidator.isValid("--", null));
    }
}
