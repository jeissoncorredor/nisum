package com.nisum.util;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class EncryptTest {

    @Test
    void encryptMd5() {
        Assertions.assertEquals("5f4dcc3b5aa765d61d8327deb882cf99", Encrypt.encodeMd5("password"));
        Assertions.assertEquals("", Encrypt.encodeMd5(" "));
        Assertions.assertEquals("", Encrypt.encodeMd5(null));
    }
}
