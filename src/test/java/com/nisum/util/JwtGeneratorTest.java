package com.nisum.util;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.util.ReflectionTestUtils;

@SpringBootTest
public class JwtGeneratorTest {

    @InjectMocks
    JWTGenerator jwtGenerator;

    @Test
    void generateValidToken() {
        ReflectionTestUtils.setField(jwtGenerator, "secret", "H+MbQeThWmZq4t7w!z%C*F-JaNcRfUjX");
        ReflectionTestUtils.setField(jwtGenerator, "time", 1800000L);
        String response = jwtGenerator.generateToken("anymail@hotmail.com");
        Assertions.assertNotEquals("", response);
    }
}
