# Nisum Test

Project created with spring boot framework using Java and Gradle

### RUN

```
gradlew bootRun
```

### Test

```
gradlew test
```

### Url
```
url: http://localhost:8080/
```

### Postman Collection
```
https://www.getpostman.com/collections/168aeee5c378240e4a40
```

###Script DB
####You can find the db script on src/main/resources/data.sql
